﻿using System;
using System.Windows.Forms;

namespace LogReader
{
	sealed class Program
	{
		[STAThread]
		static void Main(string[] args) {
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(args.Length > 0 ? new MainForm(args[0]) : new MainForm());
		}
	}
}
