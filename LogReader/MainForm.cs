﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SandWar.Logging;
using SandWar.Tools.Debug;
using LogReader.Parser;

namespace LogReader
{
	public partial class MainForm : Form
	{
		Dictionary<LogLevel, bool> filters = new Dictionary<LogLevel, bool>();
		bool othersFilter = true;
		
		LogParser log;
		int loadedEntries;
		string loadedLog;
		
		public MainForm(string log) : this() {
			if (log != null)
				LoadLog(log);
		}
		
		public MainForm() {
			InitializeComponent();
			
			foreach (var val in Enum.GetValues(typeof(LogLevel)).Cast<LogLevel>()) {
				var option = new ToolStripMenuItem(
					val.ToString(),
					null,
					(sender, e) => {
						filters[val] = ((ToolStripMenuItem)sender).Checked;
						UpdateFilters();
						toolStripStatusLabel.Text = "Filter applied.";
					}
				) {
					CheckOnClick = true,
					Checked = true
				};
				
				filters[val] = true;
				
				filterToolStripMenuItem.DropDownItems.Add(option);
			}
			
			filterToolStripMenuItem.DropDownItems.Add(
					new ToolStripMenuItem(
					"Others",
					null,
					(sender, e) => {
						othersFilter = ((ToolStripMenuItem)sender).Checked;
						UpdateFilters();
						toolStripStatusLabel.Text = "Filter applied.";
					}
				) {
					CheckOnClick = true,
					Checked = true
				}
			);
		}
		
		void UpdateFilters() {
			var currentCursor = Cursor;
			Cursor = Cursors.WaitCursor;
			
			foreach (var row in dataGridView.Rows.Cast<DataGridViewRow>())
				UpdateFilter(row);
			
			Cursor = currentCursor;
		}
		
		void UpdateFilter(DataGridViewRow row) {
			var severity = log.GetEntry(row.Index).Level;//(LogLevel)row.Cells[2].Value;
			row.Visible = filters.ContainsKey(severity) ? filters[severity] : othersFilter;
			
			if (row.Visible) {
				switch (severity) {
				case LogLevel.Debug:
				case LogLevel.Trace:
					row.DefaultCellStyle.ForeColor = Color.Gray;
					row.DefaultCellStyle.BackColor = Color.White;
					break;
				case LogLevel.Warning:
					row.DefaultCellStyle.ForeColor = Color.Black;
					row.DefaultCellStyle.BackColor = Color.Yellow;
					break;
				case LogLevel.Error:
				case LogLevel.Fatal:
					row.DefaultCellStyle.ForeColor = Color.White;
					row.DefaultCellStyle.BackColor = Color.Red;
					break;
				case LogLevel.Info:
				default:
					row.DefaultCellStyle.ForeColor = Color.Black;
					row.DefaultCellStyle.BackColor = Color.White;
					break;
				}
			}
		}
		
		void DataGridViewCellDoubleClick(object sender, DataGridViewCellEventArgs e) {
			// prevent exception if user double-clicks the header
			if (e.RowIndex < 0) return;
			
			var cells = dataGridView.Rows[e.RowIndex].Cells;
			
			var serializationData = cells[4].Value;
			
			MessageBox.Show(
				string.Format(
					"Log line: {0}\nDate: {1}\nSeverity: {2}\nMessage: {3}{4}",
					cells[0].Value,
					cells[1].Value,
					cells[2].Value,
					cells[3].Value,
					String.IsNullOrEmpty((string)serializationData) ? string.Empty : "\n\n"+serializationData
				),
				"Log entry",
				MessageBoxButtons.OK,
				MessageBoxIcon.None
			);
		}
		
		void OpenToolStripMenuItemClick(object sender, EventArgs e){
			if (openFileDialog.ShowDialog() == DialogResult.OK)
				LoadLog(openFileDialog.FileName);
		}
		
		void LoadLog(string file) {
			dataGridView.Rows.Clear();
			log = new LogParser(file, Encoding.Default);
			loadedEntries = 0;
			loadedLog = file;
			
			AddRows(200);
		}
		
		bool SetRow(DataGridViewRow row) {
			if (log == null) return false;
			
			var entry = log.GetEntry(row.Index);
			if (entry == null) return false;
			
			UpdateFilter(row);
			
			return true;
		}
		
		bool SetCell(int row, int column, out object value) {
			value = null;
			
			if (log == null) return false;
			
			var entry = log.GetEntry(row);
			if (entry == null) return false;
			
			switch(column) {
			case 0:
				value = entry.Line;
				break;
			case 1:
				value = entry.Date;
				break;
			case 2:
				value = entry.Level;
				UpdateFilter(dataGridView.Rows[row]);
				break;
			case 3:
				value = entry.Message;
				break;
			case 4:
				value = entry.Serialization;
				break;
			default:
				throw new AssertionFailedException();
			}
			
			return true;
		}
		
		void AddRows(int count) {
			UseWaitCursor = true;
			
			toolStripStatusLabel.Text = "Loading entries...";
			
			var initial = dataGridView.Rows.Count;
			for (var i = initial; i < initial + count; i++) {
				var ri = dataGridView.Rows.Add();
				if (SetRow(dataGridView.Rows[ri])) {
					//dataGridView.Rows[ri].Cells.
				} else {
					dataGridView.Rows.RemoveAt(ri);
					break;
				}
				loadedEntries++;
			}
			
			if (loadedEntries == initial)
				toolStripStatusLabel.Text = "Nothing new in the log.";
			else
				toolStripStatusLabel.Text = "Parsed " + loadedEntries + " log entries.";
			
			if(loadedEntries == 0) {
				if (loadedLog == null) {
					toolStripStatusLabel.Text = "No log file selected.";
				} else {
					toolStripStatusLabel.Text = "Unable to find log-like data in the selected file.";
					
					MessageBox.Show(
						"Unable to find log-like data in the selected file.",
						"Error",
						MessageBoxButtons.OK,
						MessageBoxIcon.Error
					);
				}
			}
			
			UseWaitCursor = false;
		}
		
		void ExitToolStripMenuItemClick(object sender, EventArgs e) {
			Close();
		}
		
		void DataGridViewNewRowNeeded(object sender, DataGridViewRowEventArgs e) {
			SetRow(e.Row);
		}
		
		void DataGridViewCellValueNeeded(object sender, DataGridViewCellValueEventArgs e) {
			object value;
			SetCell(e.RowIndex, e.ColumnIndex, out value);
			e.Value = value;
		}
		
		void MoreToolStripMenuItemClick(object sender, EventArgs e) {
			AddRows(100);
		}
		
		void RefreshToolStripMenuItemClick(object sender, EventArgs e) {
			if (loadedLog != null)
				LoadLog(loadedLog);
		}
	}
}
