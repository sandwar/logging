﻿using System;
using SandWar.Logging;

namespace LogReader.Parser
{
	[Serializable]
	public class LogEntry
	{
		public int Line;
		public DateTime Date;
		public LogLevel Level;
		public string Message;
		public string Serialization;
	}
}
