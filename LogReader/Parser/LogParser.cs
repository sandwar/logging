﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using SandWar.Logging;
using SandWar.Tools;
using SandWar.Tools.IO;

namespace LogReader.Parser
{
	public class LogParser
	{
		// max entries count in a LogPage
		const int PageSize = 256;
		
		readonly List<LogEntry> emptyList = new List<LogEntry>();
		
		class LogPage {
			public int InitialLine;
			public int FinalLine;
			public int Start;
			public int End;
			
			// this might be collected by GC at any time
			public WeakReference<List<LogEntry>> EntriesWeak;
			// this will not be collected, must be set to null after retrieval
			public List<LogEntry> EntriesStrong;
		}
		
		readonly List<LogPage> pages = new List<LogPage>();
		
		readonly string logFile;
		readonly Encoding encoding;
		
		public LogParser(string logFile, Encoding encoding) {
			this.logFile = logFile;
			this.encoding = encoding;
		}
		
		public LogEntry GetEntry(int index) {
			var page = (int)Math.Floor((float)index / PageSize);
			var entryInPage = index - (page * PageSize);
			
			// load page, skip cache if this is the last page
			var pageContent = getPage(page + 1, pages.Count - 1 == page);
			var entryContent = pageContent.EntriesStrong.Count <= entryInPage ? null : pageContent.EntriesStrong[entryInPage];
			pageContent.EntriesStrong = null; // allow collection
			return entryContent;
		}
		
		LogPage getPage(int page, bool skipCache = false) {
			if (page == 0) {
				if (pages.Count > 0) {
					pages[0].EntriesStrong = emptyList;
					return pages[0];
				}
				
				var firstPage = new LogPage() {
					InitialLine = 0,
					FinalLine = 0,
					Start = 0,
					End = 0,
					EntriesStrong = new List<LogEntry>()
				};
				
				if (pages.Count < 1) pages.Add(null);
				pages[0] = firstPage;
				return firstPage;
			}
			
			List<LogEntry> entries;
			if (!skipCache && pages.Count > page && pages[page] != null && pages[page].EntriesWeak.TryGetValue(out entries)) {
				// the page had already been parsed and was still present in cache
				pages[page].EntriesStrong = entries;
				return pages[page];
			}
			
			if (pages.Count > page - 1 && pages[page - 1] != null) {
				var previousPage = getPage(page - 1);
				var thisPage = parsePage(page, previousPage.End, previousPage.FinalLine);
				while(pages.Count <= page) pages.Add(null);
				pages[page] = thisPage;
				return thisPage;
			}
			
			// FIXME: next instruction might cause a stack overflow if page is too distant from last loaded page
			getPage(page - 1);  // force reading previous page
			return getPage(page);
		}
		
		LogPage parsePage(int page, int start, int startLine) {
			using(var s = new FileStream(logFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
				s.Position = start;
				using(var sr = new AdvancedStreamReader(s, encoding, false, 4096)) {
					int linesRead;
					var entries = readPage(sr, PageSize, startLine + 1, out linesRead);
					
					return new LogPage() {
						InitialLine = startLine + 1,
						FinalLine = startLine + linesRead,
						Start = start,
						End = (int)sr.BaseStreamPosition,
						EntriesWeak = entries,
						EntriesStrong = entries
					};
				}
			}
		}
		
		List<LogEntry> readPage(AdvancedStreamReader log, int entriesCount, int firstLine, out int linesRead) {
			var entries = new List<LogEntry>();
			
			bool somethingParsed = false;
			int lastLineNumber = 0;
			DateTime lastDate = default(DateTime);
			LogLevel lastLogLevel = default(LogLevel);
			string lastMessage = null;
			var serializedData = new StringBuilder();
			
			string line;
			int lineNumber = -1;
			// entriesCount > 1 and not > 0 because there is also one out of the while loop
			while(entriesCount > 1 && (line = log.ReadLine()) != null) {
				lineNumber++;
				var firstToken = line.Split(new [] { '\t' }, 2)[0];
				
				DateTime parsedDate;
				if(DateTime.TryParseExact(firstToken, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out parsedDate)) {
					// managed to parse first token as a date... current line must be a new element
					var allTokens = line.Split(new [] { '\t' }, 3);
					
					if(allTokens.Length == 3) {
						if (somethingParsed) {
							entriesCount--;
							entries.Add(new LogEntry() {
								Line = lastLineNumber,
								Date = lastDate,
								Level = lastLogLevel,
								Message = lastMessage,
								Serialization = serializedData.ToString()
							});
						}
						
						lastLineNumber = lineNumber + firstLine;
						lastDate = parsedDate;
						try {
							lastLogLevel = (LogLevel)Enum.Parse(typeof(LogLevel), allTokens[1]);
						} catch(ArgumentException) {
							lastLogLevel = default(LogLevel);
						}
						lastMessage = allTokens[2];
						serializedData.Remove(0, serializedData.Length);
						somethingParsed = true;
						continue;
					}
				}
				
				// failed to parse first token as a date or allTokens.Length not 3...
				// ...current line must be continuation of last element's message
				serializedData.Append(line).Append('\n');
			}
			
			if (somethingParsed) {
				entriesCount--;
				entries.Add(new LogEntry() {
					Line = lastLineNumber,
					Date = lastDate,
					Level = lastLogLevel,
					Message = lastMessage,
					Serialization = serializedData.ToString()
				});
			}
			
			linesRead = lineNumber + 1;
			return entries;
		}
	}
}
