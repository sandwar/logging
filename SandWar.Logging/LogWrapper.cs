﻿using System;
using System.Collections.Generic;
using System.Linq;
using SandWar.Tools;

namespace SandWar.Logging
{
	/// <summary>Allows to wrap multiple logs into one.</summary>
	public sealed class LogWrapper : ILog
	{
		readonly LogLevel minSeverity;
		readonly ILog[] logs;
		
		/// <summary>Indicates if the log contains at least one error or warning message.</summary>
		public bool ContainsErrors {
			get { return logs.Any(log => log.ContainsErrors); }
		}
		
		/// <summary>Wrap logs.</summary>
		/// <param name="minimumSeverity">Minimum level of severity to process messages.</param>
		/// <param name="logs">The logs to wrap.</param>
		public LogWrapper(LogLevel minimumSeverity, params ILog[] logs) {
			this.minSeverity = minimumSeverity;
			this.logs = logs;
		}
		
		/// <summary>Wrap logs.</summary>
		/// <param name="logs">The logs to wrap.</param>
		public LogWrapper(params ILog[] logs) : this(LogLevel.Debug, logs) { }
		
		/// <summary>Log a debug message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		public void Debug(string message, object obj = null) {
			if(minSeverity > LogLevel.Debug)
				return;
			
			List<Exception> es = null;
			foreach(var log in logs) {
				try {
					log.Debug(message, obj);
				} catch(Exception e) {
					if(es == null)
						es = new List<Exception>();
					
					es.Add(e);
				}
			}
			
			if(es != null) {
				if(es.Count == 1)
					throw es[1];
				
				throw new AggregateException(es);
			}
		}
		
		/// <summary>Log a trace message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		public void Trace(string message, object obj = null) {
			if(minSeverity > LogLevel.Trace)
				return;
			
			List<Exception> es = null;
			foreach(var log in logs) {
				try {
					log.Trace(message, obj);
				} catch(Exception e) {
					if(es == null)
						es = new List<Exception>();
					
					es.Add(e);
				}
			}
			
			if(es != null) {
				if(es.Count == 1)
					throw es[1];
				
				throw new AggregateException(es);
			}
		}
		
		/// <summary>Log an informational message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		public void Info(string message, object obj = null) {
			if(minSeverity > LogLevel.Info)
				return;
			
			List<Exception> es = null;
			foreach(var log in logs) {
				try {
					log.Info(message, obj);
				} catch(Exception e) {
					if(es == null)
						es = new List<Exception>();
					
					es.Add(e);
				}
			}
			
			if(es != null) {
				if(es.Count == 1)
					throw es[1];
				
				throw new AggregateException(es);
			}
		}
		
		/// <summary>Log a warning message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		public void Warning(string message, object obj = null) {
			if(minSeverity > LogLevel.Warning)
				return;
			
			List<Exception> es = null;
			foreach(var log in logs) {
				try {
					log.Warning(message, obj);
				} catch(Exception e) {
					if(es == null)
						es = new List<Exception>();
					
					es.Add(e);
				}
			}
			
			if(es != null) {
				if(es.Count == 1)
					throw es[1];
				
				throw new AggregateException(es);
			}
		}
		
		/// <summary>Log an error message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		public void Error(string message, object obj = null) {
			if(minSeverity > LogLevel.Error)
				return;
			
			List<Exception> es = null;
			foreach(var log in logs) {
				try {
					log.Error(message, obj);
				} catch(Exception e) {
					if(es == null)
						es = new List<Exception>();
					
					es.Add(e);
				}
			}
			
			if(es != null) {
				if(es.Count == 1)
					throw es[1];
				
				throw new AggregateException(es);
			}
		}
		
		/// <summary>Log a fatal error message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		public void Fatal(string message, object obj = null) {
			if(minSeverity > LogLevel.Fatal)
				return;
			
			List<Exception> es = null;
			foreach(var log in logs) {
				try {
					log.Fatal(message, obj);
				} catch(Exception e) {
					if(es == null)
						es = new List<Exception>();
					
					es.Add(e);
				}
			}
			
			if(es != null) {
				if(es.Count == 1)
					throw es[1];
				
				throw new AggregateException(es);
			}
		}
		
		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public void Dispose() {
			List<Exception> es = null;
			foreach(var log in logs) {
				try {
					log.Dispose();
				} catch(Exception e) {
					if(es == null)
						es = new List<Exception>();
					
					es.Add(e);
				}
			}
			
			if(es != null) {
				if(es.Count == 1)
					throw es[1];
				
				throw new AggregateException(es);
			}
		}
	}
}
