﻿using System;

namespace SandWar.Logging
{
	/// <summary>Indentifies a class that provides logging utilities.</summary>
	public interface ILog : IDisposable
	{
		/// <summary>Indicates if the log contains at least one error or warning message.</summary>
		bool ContainsErrors { get; }
		
		/// <summary>Log a debug message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		void Debug(string message, object obj = null);
		
		/// <summary>Log a trace message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		void Trace(string message, object obj = null);
		
		/// <summary>Log an informational message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		void Info(string message, object obj = null);
		
		/// <summary>Log a warning message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		void Warning(string message, object obj = null);
		
		/// <summary>Log an error message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		void Error(string message, object obj = null);
		
		/// <summary>Log a fatal error message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		void Fatal(string message, object obj = null);
	}
}
