﻿using System;
using System.Diagnostics;

namespace SandWar.Logging
{
	/// <summary>A class that provides high-level logging utilities.</summary>
	public sealed class Log : IDisposable
	{
		static readonly Log nullLog = new Log();
		/// <summary>Get an instance of the fake log.</summary>
		public static Log Null {
			get { return nullLog; }
		}
		
		readonly ILog destination;
		
		/// <summary>Indicates if the log contains at least one error or warning message.</summary>
		public bool ContainsErrors {
			get { return destination.ContainsErrors; }
		}
		
		/// <summary>Wrap low-level logs in a high-level container.</summary>
		/// <param name="logs">The low-level logs to wrap.</param>
		public Log(params ILog[] logs) {
			switch (logs.Length) {
			case 0:
				destination = NullLog.Instance;
				break;
			case 1:
				destination = logs[0];
				break;
			default:
				destination = new LogWrapper(logs);
				break;
			}
		}
		
		/// <summary>Log a debug message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		[Conditional("DEBUG")]
		public void Debug(string message, object obj = null) {
			destination.Debug(message, obj);
		}
		
		/// <summary>Log a debug message.</summary>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		/// <param name="format">A composite format string.</param>
		/// <param name="args"> An object array that contains zero or more objects to format.</param>
		[Conditional("DEBUG")]
		public void DebugSerializeFormat(object obj, string format, params object[] args) {
			Debug(String.Format(format, args), obj);
		}
		
		/// <summary>Log a debug message.</summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="args"> An object array that contains zero or more objects to format.</param>
		[Conditional("DEBUG")]
		public void DebugFormat(string format, params object[] args) {
			DebugSerializeFormat(null, format, args);
		}
		
		/// <summary>Log a trace message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		[Conditional("TRACE")]
		public void Trace(string message, object obj = null) {
			destination.Trace(message, obj);
		}
		
		/// <summary>Log a trace message.</summary>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		/// <param name="format">A composite format string.</param>
		/// <param name="args"> An object array that contains zero or more objects to format.</param>
		[Conditional("TRACE")]
		public void TraceSerializeFormat(object obj, string format, params object[] args) {
			Trace(String.Format(format, args), obj);
		}
		
		/// <summary>Log a trace message.</summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="args"> An object array that contains zero or more objects to format.</param>
		[Conditional("TRACE")]
		public void TraceFormat(string format, params object[] args) {
			TraceSerializeFormat(null, format, args);
		}
		
		/// <summary>Log an informational message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		public void Info(string message, object obj = null) {
			destination.Info(message, obj);
		}
		
		/// <summary>Log an informational message.</summary>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		/// <param name="format">A composite format string.</param>
		/// <param name="args"> An object array that contains zero or more objects to format.</param>
		public void InfoSerializeFormat(object obj, string format, params object[] args) {
			Info(String.Format(format, args), obj);
		}
		
		/// <summary>Log an informational message.</summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="args"> An object array that contains zero or more objects to format.</param>
		public void InfoFormat(string format, params object[] args) {
			InfoSerializeFormat(null, format, args);
		}
		
		/// <summary>Log a warning message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		public void Warning(string message, object obj = null) {
			destination.Warning(message, obj);
		}
		
		/// <summary>Log a warning message.</summary>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		/// <param name="format">A composite format string.</param>
		/// <param name="args"> An object array that contains zero or more objects to format.</param>
		public void WarningSerializeFormat(object obj, string format, params object[] args) {
			Warning(String.Format(format, args), obj);
		}
		
		/// <summary>Log a warning message.</summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="args"> An object array that contains zero or more objects to format.</param>
		public void WarningFormat(string format, params object[] args) {
			WarningSerializeFormat(null, format, args);
		}
		
		/// <summary>Log an error message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		public void Error(string message, object obj = null) {
			destination.Error(message, obj);
		}
		
		/// <summary>Log an error message.</summary>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		/// <param name="format">A composite format string.</param>
		/// <param name="args"> An object array that contains zero or more objects to format.</param>
		public void ErrorSerializeFormat(object obj, string format, params object[] args) {
			Error(String.Format(format, args), obj);
		}
		
		/// <summary>Log an error message.</summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="args"> An object array that contains zero or more objects to format.</param>
		public void ErrorFormat(string format, params object[] args) {
			ErrorSerializeFormat(null, format, args);
		}
		
		/// <summary>Log a fatal error message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		public void Fatal(string message, object obj = null) {
			destination.Fatal(message, obj);
		}
		
		/// <summary>Log a fatal error message.</summary>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		/// <param name="format">A composite format string.</param>
		/// <param name="args"> An object array that contains zero or more objects to format.</param>
		public void FatalSerializeFormat(object obj, string format, params object[] args) {
			Fatal(String.Format(format, args), obj);
		}
		
		/// <summary>Log a fatal error message.</summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="args"> An object array that contains zero or more objects to format.</param>
		public void FatalFormat(string format, params object[] args) {
			FatalSerializeFormat(null, format, args);
		}
		
		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public void Dispose() {
			destination.Dispose();
		}
	}
}
