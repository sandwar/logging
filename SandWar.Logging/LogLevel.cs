﻿using System;

namespace SandWar.Logging
{
	/// <summary>Defines the importance level of a log message.</summary>
	public enum LogLevel
	{
		/// <summary>Debug message, low importance. Only logged if DEBUG directive is defined, usually only in debug builds.</summary>
		Debug = 1,
		
		/// <summary>Trace message, low importance. Only logged if TRACE directive is defined, usually yes by default.</summary>
		Trace,
		
		/// <summary>Informational message, this is the default.</summary>
		Info,
		
		/// <summary>Warning message, high importance.</summary>
		Warning,
		
		/// <summary>Error message, very high importance.</summary>
		Error,
		
		/// <summary>Fatal error message, extremely high importance.</summary>
		Fatal
	}
}
