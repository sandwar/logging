﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using SandWar.Serialization;
using SandWar.Logging.Internal;

namespace SandWar.Logging
{
	/// <summary>A class that allows logging to file.</summary>
	public sealed class FileLog : AbstractLog
	{
		readonly StreamWriter logWriter;
		readonly IStringSerializer serializer;
		
		/// <summary>Initialize a new file log.</summary>
		/// <param name="path">File path to write log to, can be null.</param>
		/// <param name="minimumSeverity">Minimum level of severity to save messages to file.</param>
		/// <param name="encoding">The character encoding to use.</param>
		public FileLog(string path, LogLevel minimumSeverity, Encoding encoding) : base(minimumSeverity) {
			if(!Directory.GetParent(path).Exists)
				Directory.GetParent(path).Create();
			
			this.logWriter = new StreamWriter(new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.Read), encoding);
			
			this.serializer = new DebugObjectSerializer();
			
			AppDomain.CurrentDomain.ProcessExit += disposeHandler;
		}
		
		/// <summary>Initialize a new file log.</summary>
		/// <param name="path">File path to write log to, can be null.</param>
		/// <param name="minimumSeverity">Minimum level of severity to save messages to file.</param>
		public FileLog(string path, LogLevel minimumSeverity) : this(path, minimumSeverity, Encoding.Default) { }
		
		/// <summary>Initialize a new file log.</summary>
		/// <param name="path">File path to write log to, can be null.</param>
		public FileLog(string path) : this(path, LogLevel.Debug) { }
		
		///<summary>Log an error message.</summary>
		///<param name="level">The severity level of the message.</param>
		///<param name="message">The message to log.</param>
		///<param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		protected override void Log(LogLevel level, string message, object obj) {
			var date = DateTime.UtcNow.ToString("G");
			
			var exceptionMessage = string.Empty;
			var stackTrace = string.Empty;
			var ex = obj as Exception;
			if(ex != null) {
				exceptionMessage = ex.Message;
				stackTrace = ex.StackTrace;
			} else if(level >= LogLevel.Warning) {
				stackTrace = new StackTrace(2, true).ToString();
			}
			
			var serialized = string.Empty;
			if(obj != null) {
				try {
					serialized = serializer.Serialize(obj);
				} catch {
					serialized = obj.ToString();
				}
			}
			
			// date type message [ LF exceptionMessage ] [ LF stackTrace ] LF serialized
			// LEN  LEN  LEN     [ 1   LEN             ] [ 1   LEN       ] 1   LEN
			var sb = new StringBuilder(date.Length + level.ToString().Length + message.Length + exceptionMessage.Length + stackTrace.Length + serialized.Length + 17);
			
			sb.Append(date);
			sb.Append('\t').Append(level).Append('\t');
			sb.Append(message);
			
			if(exceptionMessage.Length > 0)
				sb.Append("\n").Append(exceptionMessage);
			
			if(stackTrace.Length > 0)
				sb.Append("\n").Append(stackTrace);
			
			if(obj != null)
				sb.Append("\n").Append(serialized);
			
			var output = sb.ToString();
			saveLog(output, level >= LogLevel.Warning);
		}
		
		void saveLog(string message, bool important) {
			lock(logWriter) {
				if(important) {
					logWriter.WriteLine(message);
					logWriter.Flush();
				} else {
					logWriter.WriteLine(message);
					//logWriter.Flush();
				}
			}
		}
		
		volatile bool disposed = false;
		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public override void Dispose() {
			base.Dispose();
			lock(logWriter) {
				if(disposed)
					return;
				
				disposed = true;
				
				logWriter.Flush();
				logWriter.Dispose();
				
				AppDomain.CurrentDomain.ProcessExit -= disposeHandler;
			}
		}
		
		void disposeHandler(object sender, EventArgs e) {
			Dispose();
		}
	}
}
