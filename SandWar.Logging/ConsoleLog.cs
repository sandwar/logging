﻿using System;

namespace SandWar.Logging
{
	/// <summary>A class that allows logging to console.</summary>
	public sealed class ConsoleLog : AbstractLog
	{
		/// <summary>Initialize a new console log.</summary>
		/// <param name="minimumSeverity">Minimum level of severity to print messages to console.</param>
		public ConsoleLog(LogLevel minimumSeverity) : base(minimumSeverity) { }
		
		/// <summary>Log an error message.</summary>
		/// <param name="level">The severity level of the message.</param>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the message.</param>
		protected override void Log(LogLevel level, string message, object obj) {
			string type;
			switch(level) {
			case LogLevel.Debug:
				type = "Debug";
				break;
			
			case LogLevel.Trace:
				type = "Trace";
				break;
			
			case LogLevel.Warning:
				type = "Warning";
				break;
			
			case LogLevel.Error:
				type = "Error";
				break;
			
			case LogLevel.Fatal:
				type = "Fatal";
				break;
			
			default:
			case LogLevel.Info:
				type = "Info";
				break;
			}
			
			Console.WriteLine("{0}\t{1}: {2}", DateTime.Now.ToString("G"), type, message);
		}
	}
}
