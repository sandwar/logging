﻿using System;

namespace SandWar.Logging
{
	/// <summary>A fake log that does nothing.</summary>
	public sealed class NullLog : ILog
	{
		static readonly ILog nullLog = new NullLog();
		/// <summary>Get an instance of the fake log.</summary>
		public static ILog Instance {
			get { return nullLog; }
		}
		
		NullLog() { }
		
		/// <summary>Does nothing.</summary>
		/// <param name="message">Ignored parameter.</param>
		/// <param name="obj">Ignored parameter.</param>
		public void Trace(string message, object obj = null) { }
		
		/// <summary>Does nothing.</summary>
		/// <param name="message">Ignored parameter.</param>
		/// <param name="obj">Ignored parameter.</param>
		public void Debug(string message, object obj = null) { }
		
		/// <summary>Does nothing.</summary>
		/// <param name="message">Ignored parameter.</param>
		/// <param name="obj">Ignored parameter.</param>
		public void Info(string message, object obj = null) { }
		
		/// <summary>Does nothing.</summary>
		/// <param name="message">Ignored parameter.</param>
		/// <param name="obj">Ignored parameter.</param>
		public void Warning(string message, object obj = null) { }
		
		/// <summary>Does nothing.</summary>
		/// <param name="message">Ignored parameter.</param>
		/// <param name="obj">Ignored parameter.</param>
		public void Error(string message, object obj = null) { }
		
		/// <summary>Does nothing.</summary>
		/// <param name="message">Ignored parameter.</param>
		/// <param name="obj">Ignored parameter.</param>
		public void Fatal(string message, object obj = null) { }
		
		/// <summary>Always false.</summary>
		public bool ContainsErrors {
			get { return false; }
		}
		
		/// <summary>Does nothing.</summary>
		public void Dispose() { }
	}
}
