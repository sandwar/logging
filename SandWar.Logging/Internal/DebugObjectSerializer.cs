﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using SandWar.Serialization;

namespace SandWar.Logging.Internal
{
	class DebugObjectSerializer : IStringSerializer {
		
		const int MaxRecursion = 4;
		
		const string KeyValueSeparator= ": ";
		const string FieldSeparator = "\n";
		const string Indentation = "\t";
		const string OverflowSymbol = "...";
		const string ExceptionSymbol = "????";
		const string ReferenceSymbol = "*reference";
		
		public string Serialize<T>(T obj) {
			var sb = new StringBuilder();
			var references = new HashSet<object>() { obj };
			print(sb, obj, 0, references);
			return sb.ToString();
		}
		
		public T Deserialize<T>(string str, T obj) {
			throw new NotSupportedException();
		}
		
		static void print<T>(StringBuilder sb, T obj, int indentation, HashSet<object> referenced) {
			var tillNow = sb.Length;
			
			try {
				if (indentation > MaxRecursion) {
					sb.Append(OverflowSymbol);
					return;
				}
				
				if (indentation > 0)
					sb.Append(FieldSeparator);
				
				bool first = true;
				foreach (var member in getFieldsAndProps(obj.GetType())) {
					if (first)
						first = false;
					else
						nextField(sb);
					
					indent(sb, indentation);
					
					sb.Append(member.Name).Append(KeyValueSeparator);
					
					var type = getType(member);
					var value = getValue(member, obj);
					
					if (type.IsClass) {
						if (referenced.Contains(value)) {
							sb.Append(ReferenceSymbol);
							continue;
						}
						
						referenced.Add(value);
					}
					
					if (type.IsEnum) {
						sb.Append(Enum.GetName(type, value));
					} else if (hasToString(type)) {
						sb.Append(value.ToString());
					} else if (isEnumerable(type)) {
						sb.Append("//collections not supported//");
					} else {
						print(sb,value, indentation + 1, referenced);
					}
				}
			} catch(Exception) {
				sb.Length = tillNow;
				sb.Append(ExceptionSymbol);
			}
		}
		
		static void nextField(StringBuilder sb) {
			sb.Append(FieldSeparator);
		}
		
		static void indent(StringBuilder sb, int indentation) {
			for (var i = 0; i < indentation; i++)
				sb.Append(Indentation);
		}
		
		static bool hasToString(Type t) {
			var method = t.GetMethod("ToString", new Type[0]);
			return method != null && method.DeclaringType == t;
		}
		
		static IEnumerable<MemberInfo> getFieldsAndProps(Type t) {
			var bindingFlags = BindingFlags.Public | BindingFlags.Instance;
			return t.GetFields(bindingFlags).Cast<MemberInfo>().Concat(t.GetProperties(bindingFlags));
		}
		
		static Type getType(MemberInfo member) {
			return member is FieldInfo ?
				((FieldInfo)member).FieldType :
				((PropertyInfo)member).PropertyType;
		}
		
		static readonly object[] emptyArray = new object[0];
		static object getValue(MemberInfo member, object obj) {
			return member is FieldInfo ?
				((FieldInfo)member).GetValue(obj) :
				((PropertyInfo)member).GetValue(obj, emptyArray);
		}
		
		static bool isEnumerable(Type t) {
			return t.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>));
		}
	}
}
