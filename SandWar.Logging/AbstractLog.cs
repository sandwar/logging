﻿using System;

namespace SandWar.Logging
{
	/// <summary>Provides a way to quickly implement a logging utility.</summary>
	public abstract class AbstractLog : ILog, IDisposable
	{
		readonly LogLevel minSeverity;
		
		/// <summary>Initialize a new log.</summary>
		protected AbstractLog() : this(LogLevel.Debug) { }
		
		/// <summary>Initialize a new log.</summary>
		/// <param name="minimumSeverity">Minimum level of severity to print messages to console.</param>
		protected AbstractLog(LogLevel minimumSeverity) {
			this.minSeverity = minimumSeverity;
		}
		
		volatile bool containsErrors;
		/// <summary>Indicates if the log contains at least one error or warning message.</summary>
		public bool ContainsErrors {
			get { return containsErrors; }
		}
		
		/// <summary>Log a debug message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the object.</param>
		public void Debug(string message, object obj = null) {
			if(minSeverity > LogLevel.Debug)
				return;
			
			try {
				Log(LogLevel.Debug, message, obj);
			} catch(Exception ex) {
				Warning("Failed to log Debug: "+message, ex);
			}
		}
		
		/// <summary>Log a trace message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the object.</param>
		public void Trace(string message, object obj = null) {
			if(minSeverity > LogLevel.Trace)
				return;
			
			try {
				Log(LogLevel.Trace, message, obj);
			} catch(Exception ex) {
				Warning("Failed to log Trace: "+message, ex);
			}
		}
		
		/// <summary>Log an informational message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the object.</param>
		public void Info(string message, object obj = null) {
			if(minSeverity > LogLevel.Info)
				return;
			
			try {
				Log(LogLevel.Info, message, obj);
			} catch(Exception ex) {
				Warning("Failed to log Info: "+message, ex);
			}
		}
		
		/// <summary>Log a warning message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the object.</param>
		public void Warning(string message, object obj = null) {
			if(minSeverity > LogLevel.Warning)
				return;
			
			try {
				containsErrors = true;
				Log(LogLevel.Warning, message, obj);
			} catch(Exception ex) {
				Error("Failed to log Warning", ex);
			}
		}
		
		/// <summary>Log an error message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the object.</param>
		public void Error(string message, object obj = null) {
			if(minSeverity > LogLevel.Error)
				return;
			
			try {
				containsErrors = true;
				Log(LogLevel.Error, message, obj);
			} catch(Exception ex) {
				Fatal("Failed to log Error", ex);
			}
		}
		
		/// <summary>Log a fatal error message.</summary>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the object.</param>
		public void Fatal(string message, object obj = null) {
			if(minSeverity > LogLevel.Fatal)
				return;
			
			containsErrors = true;
			Log(LogLevel.Fatal, message, obj);
		}
		
		/// <summary>Log an error message.</summary>
		/// <param name="level">The severity level of the message.</param>
		/// <param name="message">The message to log.</param>
		/// <param name="obj">An object or <see cref="Exception"/> to serialize with the object.</param>
		protected abstract void Log(LogLevel level, string message, object obj);
		
		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public virtual void Dispose() { }
	}
}
